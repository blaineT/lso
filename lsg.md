# Liquid Sunshine Group

![lsg](/img/lsg.png)

## Board

* President = Steven Winslow
* Operations Director = Judy Higgins
* Finance Director = Kayla Winslow
* Communications Director = Lloyd Bledsoe
* Marketing Director = Kevan Johnson
* Fundraising Director = Jessica Earl

## 501(c)(3) Information

### Mission Statement

Liquid Sunshine Group is focused on the improvement and promotion of the sport of disc golf in the Willamette Valley. We participate in the improvement of disc golf by providing the infrastructure for the sport to grow, through the support of new disc golf course development as well as improvements to existing disc golf courses in the Salem, OR area. Liquid Sunshine Group promotes events that foster the growth of the sport and that reflect a family friendly spirit that aids in the growth of the sport, development, and professional presentation to the general public.

### Details

EIN = `84-2394231`

* [Oregon Department of Justice Registration](https://justice.oregon.gov/charities)
* [Oregon Secretary of State Business Entity Data](http://egov.sos.state.or.us/br/pkg_web_name_srch_inq.show_detl?p_be_rsn=2064830&p_srce=BR_INQ&p_print=FALSE)
* [IRS Determination Letter](https://apps.irs.gov/pub/epostcard/dl/FinalLetter_84-2394231_LIQUIDSUNSHINEGROUP_01022020_00.tif)

## Expenditures

### 501(c)(3) Transactions

| Date | Amount | Order # | Details | Payment |
| - | - | - | - | - |
| 2021-06-28 | -$50.00 | 119870262 | oregon secretary of state corporation division annual report fee | snry |
| 2021-10-01 | -$150.96 | 18835540 | name.com hosting renewal | snry |
| 2021-11-17 | -$150.00 | 5217561 | LSO 2022 sanctioning fee and certificate of insurance | snry |
| 2021-12-07 | $250.00 | | Initial MAPS deposit | snry |
| 2022-01-24 | $500 | | private donation |
| 2022-01-05 | -$40.00 | OSJOSJ000032619 | 2020 Oregon DoJ Charities Annual Filing Fee | snry |
| 2022-02-11 | -$20.00 | OSJOSJ000033608 | 2021 Oregon DoJ Charities Annual Filing Fee | snry |
| 2022-04-16 | -$320.00 | | 16x LSO 2022 Trophies | snry |
| 2022-09-21 | -$151.96 | 20618275 | name.com hosting renewal | paypal |

### Liquid Sunshine Open Transactions

| Date | Amount | Order # | Details | Payment |
| - | - | - | - | - |
| 2022-03-01 | -$334.90 | #0017 | 80x players pack minis from trash panda disc golf | paypal |
| 2022-03-02 | -$36.28 | N/A | artist changes to LSO logo to meet hot stamping requirements | snry |
| 2022-03-02 | -$338.00 | #0708 | 80x players pack hard enamel pins | paypal |
| 2022-03-03 | -$1,343.65 | #D28697 | 50x instinct, 50x essence, custom hot stamp discmania neo discs | paypal |
| 2022-03-15 | -$1,538.00 | 1392101-00 | 80x screen printed suntek umbrellas | paypal |
| 2022-04-16 | -$956.00 | #0004 | 80x 8"x12" RipitGrip pads | paypal |
| 2022-04-25 | -$1,153.00 | | LSO 2022 greens fees | maps |
