# Frequently Asked Questions

* When will the layout be available?
  * It will go out with the players guide the week of the tournament, no later than Friday.
* What time does registration open
  * 3 months before the tournament for sponsors, 2 months before the tournament for general registration.
* What is the tee order?
  * Check the [About](https://www.discgolfscene.com/tournaments/Liquid_Sunshine_Open_2023) section on the tournament page.
